#include <iostream>
using namespace std;

int main() {
	int num, step;
	cout << "Enter a number\n> ";
	cin >> num;

	// loop until we reach the end
	while (num != 1) {
		// if num is even
		if (num % 2 == 0) num = num / 2;

		// if num is odd
		else num = (num * 3) + 1;

		cout << "num is " << num << endl;
		// track how many steps this is going to take
		step++;
	}

	cout << endl << "Steps taken to reach 1: " << step << endl;
}