#include <iostream>
using namespace std;

// return text
void retText(string inp, string compans, string pl) {
	cout << "\nyour answer: " << pl << "\ncomputer answer: " << compans << "\n" << inp << "\n" << endl;
}

// find element in array
int inArray(const string value_to_find, string arr[], int arr_length) {
	for (int i = 0; i < arr_length; i++) {
		if (value_to_find == arr[i]) return i;
	}
	return -1;
}

// rock paper scissors
int main() {
	srand(time(NULL)); // seed the random number generator
	string option, compans, player_ans; // create vars
	string choices[3] = {"paper", "rock", "scissors"}; // the 3 choices
	int choices_length  = sizeof(choices) / sizeof(*choices); // length of choices
	option = ""; // making option empty str so we don't get garbage data

	// loop forever until we hit quit
	while (option.front() != 'q') {

		// get player input
		cout << "(p)lay or (q)uit?\n> ";
		cin >> player_ans;

		// if our input starts with q (quit)
		if (player_ans.front() == 'q') break;

		// are we playing? if not ask the same question on the next iteration
		else if (player_ans.front() == 'p') {
			compans = choices[rand() % 3];
			while (true) {
				cout << "rock, paper or scissors?\n> ";
				cin >> player_ans;
				if (inArray(player_ans, choices, choices_length) != -1) {
					if (compans == player_ans) {
						retText("draw.", compans, player_ans);
						break;
					} 
					else if (player_ans == choices[inArray(compans, choices,choices_length)-1%compans.length()]) {
						retText("you win!", compans, player_ans);
						break;
					}
					else {
						retText("you lose.", compans, player_ans);
						break;
					}
				}
			}
		}
	}
	return 0;
}