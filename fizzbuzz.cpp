#include <iostream>
using namespace std;
int main() {
    bool c1, c2;
    for(unsigned int i = 1; i <= 100; i++)
    {
        c1 = i % 3 == 0;
        c2 = i % 5 == 0;
        if (c1)
            cout << "fizz";
        if (c2)
            cout << "buzz";
        if (!c1 && !c2)
            cout << i;

        cout << endl;  
    }
}