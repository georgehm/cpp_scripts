#include <iostream>
using namespace std;

int main() {
	bool doors[100] = {false}; // create an array which has a length of 100, all values are false

	for (int pass = 1; pass <= 100; pass++) { // we want 100 passes of the doors, thats what this loop is for
		for(int current_door = 0; current_door < 100; current_door++) // go to each door
		{ // can probably make this for loop more efficient by not visiting the doors I don't need to go to
			if ((current_door + 1) % pass == 0) // if this is a door we should visit
				doors[current_door] = !doors[current_door]; // set the opposite value (false becomes true, vice versa)
		}
	}
	
	// return the state of the doors (1 = open, 0 = closed)
	for(int i = 0; i < (sizeof(doors)/sizeof(*doors)); i++)
	{
		if (doors[i]) cout << 1;

		else cout << 0;
	}
	cout << endl;
	
}