// testing imports, "input.cpp" uses this file.
#include <iostream>
using namespace std;

namespace imp {
	string getInput(string question) {
		string input = "";

		cout << question << endl;
		cout << "> ";

		cin >> input;

		return input;
	}
}