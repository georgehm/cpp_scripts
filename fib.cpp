#include <iostream>
using namespace std;

int main() {
	unsigned long int last_two[2] = {0, 1};
	unsigned long int fib_num;
	for(unsigned int i = 0; i < 50; i++)
	{
		if (i == 0) {
			cout << last_two[0] << endl << last_two[1] << endl;
		}
		fib_num = last_two[0] + last_two[1];
		cout << fib_num << endl;
		last_two[0] = last_two[1];
		last_two[1] = fib_num;
	}
	
	return 0;
}